
let userNumber;

do {
  userNumber = +prompt('Enter your number');
} while(Number.isNaN(userNumber) == true);

if(userNumber<5) {
  console.log('Sorry, no numbers');
}
 
for(let i = 1; i<=userNumber; i++) {
  if (i % 5 === 0) {
      console.log(i);
  } 
}
